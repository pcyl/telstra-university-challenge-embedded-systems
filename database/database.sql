CREATE DATABASE tuc;

CREATE SCHEMA smartbin;

CREATE TABLE smartbin.user
(
	user_id INTEGER NOT NULL,
	givenName VARCHAR(15) NOT NULL,
	familyName VARCHAR(15) NOT NULL,
	email VARCHAR(30) NOT NULL,
	rubbishCount INTEGER NOT NULL,
	since DATE NOT NULL,
	birthday DATE NOT NULL,

	CONSTRAINT User_pkey PRIMARY KEY (user_id)
);
