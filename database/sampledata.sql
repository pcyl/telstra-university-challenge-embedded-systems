% Sample data

INSERT INTO smartbin.user VALUES (1,'Jack','Fudd','jfudd@gmail.com',0,'2016-05-11','1994-02-28');
INSERT INTO smartbin.user VALUES (2,'Daniel','Runner','daniel.runner@gmail.com',0,'2016-03-24','1995-07-04');
INSERT INTO smartbin.user VALUES (3,'Patrick','Daffy','patdaffy@gmail.com',0,'2016-04-26','1995-03-06');
INSERT INTO smartbin.user VALUES (4,'Brendan','Wile','bwile@gmail.com',0,'2016-04-28','1996-08-24');
INSERT INTO smartbin.user VALUES (5,'Josh','Gonzales','joshgonzales@gmail.com',0,'2016-04-28','1993-08-28');
INSERT INTO smartbin.user VALUES (6,'Jimmy','Bugs','jimbugs@gmail.com',0,'2016-05-06','1994-03-13');
